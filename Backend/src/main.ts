import { Module, MiddlewareConsumer } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { CorsMiddleware } from 'Helper/cors.middleware';
import * as cors from 'cors';

@Module({
  imports: [AppModule],
})
export class MainModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(CorsMiddleware).forRoutes('*');
  }
}

async function bootstrap() {
  const app = await NestFactory.create(MainModule);
  app.use(cors());
  // Set the global prefix for all routes
  app.setGlobalPrefix('api/v1');

  // Swagger configuration
  const config = new DocumentBuilder()
    .setTitle('Star Cover')
    .setDescription('Star_Cover API')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api/v1/docs', app, document);

  await app.listen(5000);
}
bootstrap();
