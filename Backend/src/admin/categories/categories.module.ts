import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { CategoriesController } from './categories.controller';
import { CategoriesService } from './categories.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { Categories } from './categories.schema';
import { EnsureAdminAuthenticatedMiddleware } from '../../../Helper/middleware'
import { Admin } from '../auth/auth.schema';


@Module({
  imports: [
    SequelizeModule.forFeature([Categories , Admin]), 
  ],

  controllers: [CategoriesController],
  providers: [CategoriesService],
})



export class CategoryModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(EnsureAdminAuthenticatedMiddleware)
      .forRoutes(CategoriesController);    
  }
}