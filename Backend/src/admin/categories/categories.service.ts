import { BadRequestException, Injectable } from '@nestjs/common';
import { Categories } from './categories.schema'; // Assuming this is your Sequelize model
import { Model } from 'sequelize-typescript';
import { InjectModel } from '@nestjs/sequelize';
// import { Op, Sequelize } from 'sequelize';
import { Op, Sequelize } from 'sequelize';
import { Sub_Categories } from '../sub_categories/sub_categories.schema';

@Injectable()
export class CategoriesService {
  constructor(
    @InjectModel(Categories) private categoryModel: typeof Categories,
  ) {}

  async createCategory(
    reqUser: any,
    createCategoryDto: any,
  ): Promise<Categories> {
    try {
      const { category_name, status, include_store_menu } = createCategoryDto;

      // Create a new category using Sequelize's create method
      const newCategory = await this.categoryModel.create({
        category_name: category_name,
        status: status,
        include_store_menu: include_store_menu,
        created_at: new Date(),
      });

      return newCategory;
    } catch (error) {
      console.error('Error creating category:', error);
      throw new BadRequestException('Could not create category.');
    }
  }

  async categoryNameExist(reqBody) {
    try {
      const categories = await this.categoryModel.findOne({
        where: { category_name: reqBody.category_name },
        raw: true,
        nest: true,
      });

      return categories;
    } catch (error) {
      throw error;
    }
  }

  async allCategoryListing(reqbody, reqUser) {
    try {
      let order_column = reqbody.order_column || 'id';
      let sort_order = reqbody.order_direction === 'desc' ? 'DESC' : 'ASC'; // Assuming order_direction is 'desc' or 'asc'
      let filter_value = reqbody.search || '';
      let offset =
        parseInt(reqbody.per_page) * (parseInt(reqbody.current_page) - 1);
      let limit = parseInt(reqbody.per_page) || 5;

      let order = [[order_column, sort_order]]; // Properly structured order array

      let whereClause = { deleted_at: null }; // Assuming deleted_at field

      if (filter_value) {
        whereClause[Op.or] = [
          { category_name: { [Op.like]: `%${filter_value}%` } },
        ];
      }

      const { count, rows } = await Categories.findAndCountAll({
        where: whereClause,
        attributes: [
          'id',
          'category_name',
          'status',
          'include_store_menu',
          'created_at',
        ],
        offset: offset,
        // order: order,
        limit: limit,
        raw: true,
        nest: true,
      });

      return {
        totalRecords: count,
        Categorie_listing: rows,
      };
    } catch (error) {
      console.log('Error : ', error);
      throw error; // Rethrow the error to handle it in the calling code
    }
  }

  async CategoriesById(reqUser, id) {
    try {
      const data = await this.categoryModel.findOne({ where: { id , deleted_at:null } });
      return data;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  async updateCategories(reqUser, id, reqBody) {
    try {
      const updatedCategory = await this.categoryModel.update(
        {
          category_name: reqBody.category_name,
          status : reqBody.status,
          include_store_menu : reqBody.include_store_menu,
          updated_at: new Date(),
        },
        {
          returning: true,
          where: { id: id, deleted_at: null },
        },
      );

      // The updatedCategory variable contains the number of affected rows
      // and possibly the updated rows if the "returning" option is set.

      return updatedCategory;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  async deleteCategorie(id) {
    try {
      // Assuming Category is your Sequelize model
      const category = await this.categoryModel.update(
        { deleted_at: new Date() },
        {
          where: { id },
          returning: true, // To return the updated row
        },
      );

      return category;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  async allCategoryListingVL(reqUser) {
    try {
      var data = await Categories.findAll({
        where: { deleted_at: null },
        attributes: ['id', 'category_name'],
        raw: true,
        nest: true,
      });

      const valueLabelPairs = data?.map((category) => {
        return { value: category?.id, label: category?.category_name };
      });

      return valueLabelPairs;
    } catch (error) {
      console.log('Error : ', error);
      throw error; // Rethrow the error to handle it in the calling code
    }
  }

  async allCategoryListingVLWithSubcategory(reqUser) {
    try {
      var data = await Categories.findAll({
        where: { deleted_at: null },
        attributes: ['id', 'category_name'],
        raw: true,
        nest: true,
      });

      var Subdata = await Sub_Categories.findAll({
        where: { deleted_at: null },
        attributes: ['id', 'sub_category_name' , 'category_id'],
        raw: true,
        nest: true,
      });

      const valueLabelPairs = data?.map(category => {
          const categoryWithSubcategory = { value: category?.id, label: category?.category_name  ,  subCategory:[]};
          Subdata.map(subcate => {
            if (subcate.category_id === category.id) {
                categoryWithSubcategory.subCategory.push({
                    value: subcate.id,
                    label: subcate.sub_category_name
                });
            }
          });
          return categoryWithSubcategory;
      });

      return valueLabelPairs;
    } catch (error) {
      console.log('Error : ', error);
      throw error; // Rethrow the error to handle it in the calling code
    }
  }

}
