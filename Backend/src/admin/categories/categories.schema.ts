import { Column, Model, Table, DataType, Sequelize, CreatedAt, UpdatedAt, DeletedAt } from 'sequelize-typescript';

@Table({ paranoid: false , tableName: 'Category',deletedAt: "deleted_at" })
export class Categories extends Model<Categories> {

  @Column({ primaryKey: true, autoIncrement: true })
  id: number;

  @Column
  category_name: string;

  @Column
  status: number;

  @Column
  include_store_menu: number;
  
  @CreatedAt
  created_at: Date;

  @UpdatedAt
  updated_at: Date ;

  @Column
  deleted_at: Date ;
}
