import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Req,
  Res,
} from '@nestjs/common';
import { CategoriesService } from './categories.service';
import {
  CustomCatchBlockErrorMessage,
  CustomErrorResponse,
  CustomResponse,
  Success,
} from '../../../Helper/commonResponse';
import { Request, Response } from 'express';
import { CreateCategory } from './categories.interface';
import { ValidateCategory } from './Validation/categories.createValidation';
import { validate } from 'class-validator';

@Controller('categories')
export class CategoriesController {
  constructor(private readonly categoryService: CategoriesService) {}

  isValidMySQLId(id: string): boolean {
    const regex = /^\d+$/;
    return regex.test(id);
  }

  @Post('create')
  async categoriecreate(
    @Body() CreateCategory: CreateCategory,
    @Req() req: any,
    @Res() res: Response,
  ) {
    try {
      if (!req.user) {
        return new CustomErrorResponse(
          res,
          401,
          'Invalid User login',
          'Invalid Login credential',
        );
      }

      const errors = {};
      const categoryInput = new ValidateCategory();
      categoryInput.category_name = CreateCategory.category_name;
      categoryInput.include_store_menu = CreateCategory.include_store_menu;
      categoryInput.status = CreateCategory.status;

      const validation_errors = await validate(categoryInput);

      const findCategoryExist =
        await this.categoryService.categoryNameExist(CreateCategory);
      if (findCategoryExist) {
        errors['category_name'] = 'This Category name is already exist';
      }
      if (validation_errors.length > 0 || Object.keys(errors).length > 0) {
        validation_errors.map((error) => {
          errors[error['property']] = Object.values(error.constraints)[0];
          // UserController.final_error_object['errors'][error['property']] = Object.values(error.constraints)[0]
        });

        return new CustomErrorResponse(
          res,
          422,
          'Something went wrong',
          errors,
        );
      }

      const createdData = await this.categoryService.createCategory(
        req.user,
        CreateCategory,
      );
      if (createdData) {
        return new Success(
          res,
          200,
          createdData,
          '🎉 Category Created Successfully!',
        );
      } else {
        return new CustomResponse(
          res,
          400,
          createdData,
          'Something went wrong during creation',
        );
      }
    } catch (error) {
      console.log('Create Block Error -> ', error);
      return new CustomCatchBlockErrorMessage(
        res,
        500,
        error.toString(),
        'Something went wrong',
      );
    }
  }

  @Post('list')
  async getAllCategoryList(@Req() req: any, @Res() res: Response) {
    try {
      if (!req.user) {
        return new CustomErrorResponse(
          res,
          401,
          'Invalid User login',
          'Invalid Login credential',
        );
      }

      let category_listing = await this.categoryService.allCategoryListing(
        req.body,
        req.user,
      );

      if (category_listing) {
        return new Success(res, 200, category_listing, '🎉 All Categories Listed Successfully!');
      } else {
        return new CustomResponse(
          res,
          400,
          category_listing,
          'Something went wrong',
        );
      }
    } catch (error) {
      console.log('Create Block Error -> ', error);
      return new CustomCatchBlockErrorMessage(
        res,
        500,
        error.toString(),
        'Something went wrong',
      );
    }
  }

  @Get('list/:id')
  async getByIdCategoriesList(
    @Param('id') id: string,
    @Req() req: any,
    @Res() res: Response,
  ) {
    try {
      if (!req.user) {
        return new CustomErrorResponse(
          res,
          401,
          'Invalid User login',
          'Invalid Login credential',
        );
      }
      const CategoryId = req.params.id;
      if (!this.isValidMySQLId(CategoryId)) {
        return new CustomErrorResponse(
          res,
          404,
          'Enter valid Category id',
          'Category id isnot valid',
        );
      }

      const ListData = await this.categoryService.CategoriesById(req.user, id);
      if (!ListData) {
        return new CustomErrorResponse(
          res,
          500,
          'Grommet not found',
          'GrommetID doesnot found in database',
        );
      } 
      return new Success(res, 200, ListData, '🔍 Category Found Successfully!');
    } catch (error) {
      console.log('Create Block Error -> ', error);
      return new CustomCatchBlockErrorMessage(
        res,
        500,
        error.toString(),
        'Something went wrong',
      );
    }
  }

  @Post('update/:id')
  async updateCategoriesById(
    @Param('id') id: string,
    @Req() req: any,
    @Res() res: Response,
  ) {
    try {
      if (!req.user) {
        return new CustomErrorResponse(
          res,
          401,
          'Invalid User login',
          'Invalid Login credential',
        );
      }
      const CategoryId = req.params.id;
      if (!this.isValidMySQLId(CategoryId)) {
        return new CustomErrorResponse(
          res,
          404,
          'Enter valid Category id',
          'Category id isnot valid',
        );
      }

      const existingCategory = await this.categoryService.CategoriesById(req.user,CategoryId);
      if (!existingCategory) {
        return new CustomErrorResponse(
          res,
          500,
          'Category not found',
          'CategoryID doesnot found in database',
        );
      }
      const errors = {};
      const categoryInput = new ValidateCategory();
      categoryInput.category_name = req.body.category_name;
      categoryInput.status = req.body.status;
      categoryInput.include_store_menu = req.body.include_store_menu;

      const validation_errors = await validate(categoryInput);

      if (validation_errors.length > 0 || Object.keys(errors).length > 0) {
        validation_errors.map((error) => {
          errors[error['property']] = Object.values(error.constraints)[0];
          // UserController.final_error_object['errors'][error['property']] = Object.values(error.constraints)[0]
        });

        return new CustomErrorResponse(
          res,
          422,
          'Something went wrong',
          errors,
        );
      }

      const updateData = await this.categoryService.updateCategories(
        req.user,
        id,
        req.body,
      );
      if (updateData) {
        return new Success(
          res,
          200,
          true,
          '🎊 Category Updated Successfully!',
        );
      } else {
        return new CustomResponse(
          res,
          400,
          false,
          'Something went wrong during Serach',
        );
      }
    } catch (error) {
      console.log('Create Block Error -> ', error);
      return new CustomCatchBlockErrorMessage(
        res,
        500,
        error.toString(),
        'Something went wrong',
      );
    }
  }

  @Post('delete/:id')
  async categorieDeleteById(
    @Param('id') id: string,
    @Req() req: any,
    @Res() res: Response,
  ) {
    try {

      const CategoryId = req.params.id;
      if (!this.isValidMySQLId(CategoryId)) {
        return new CustomErrorResponse(
          res,
          404,
          'Enter valid Category id',
          'Category id isnot valid',
        );
      }

      const existingCategory = await this.categoryService.CategoriesById(req.user,CategoryId);
      if (!existingCategory) {
        return new CustomErrorResponse(
          res,
          500,
          'Category not found',
          'CategoryID doesnot found in database',
        );
      }
      
      const data = await this.categoryService.deleteCategorie(id);
      if (data) {
        return new Success(
          res,
          200,
          {},
          '🗑️ Category Deleted Successfully!',
        );
      } else {
        return new CustomResponse(
          res,
          400,
          data,
          'Something went wrong during Serach',
        );
      }
    } catch (error) {
      console.log('Create Block Error -> ', error);
      return new CustomCatchBlockErrorMessage(
        res,
        500,
        error.toString(),
        'Something went wrong',
      );
    }
  }

  @Get('list_V_L')
  async getAllCategoryListValueLabelWise(
    @Req() req: any,
    @Res() res: Response,
  ) {
    try {
      if (!req.user) {
        return new CustomErrorResponse(
          res,
          401,
          'Invalid User login',
          'Invalid Login credential',
        );
      }

      let category_listing_VL = await this.categoryService.allCategoryListingVL(
        req.user,
      );

      if (category_listing_VL) {
        return new Success(
          res,
          200,
          category_listing_VL,
          '📋 All Categories Listed Successfully by Label and Value!',
        );
      } else {
        return new CustomResponse(
          res,
          400,
          category_listing_VL,
          'Something went wrong',
        );
      }
    } catch (error) {
      console.log('Create Block Error -> ', error);
      return new CustomCatchBlockErrorMessage(
        res,
        500,
        error.toString(),
        'Something went wrong',
      );
    }
  }

  @Get('list_V_L_subcategpry')
  async getAllCategoryListValueLabelWiseWithSubCategories(@Req() req: any, @Res() res: Response) {
    try {
      if (!req.user) {
        return new CustomErrorResponse(
          res,
          401,
          'Invalid User login',
          'Invalid Login credential',
        );
      }
      
      let category_listing_VL_with_subcategory = await this.categoryService.allCategoryListingVLWithSubcategory(req.user); 
    
      if (category_listing_VL_with_subcategory) {
        return new Success(res, 200, category_listing_VL_with_subcategory, '📋 All Categories Listed Successfully by Label and Value with Subcategories!');
      } else {
        return new CustomResponse(
          res,
          400,
          category_listing_VL_with_subcategory,
          'Something went wrong',
        );
      }
    } catch (error) {
      console.log('Create Block Error -> ', error);
      return new CustomCatchBlockErrorMessage(
        res,
        500,
        error.toString(),
        'Something went wrong',
      );
    }
  }

}
