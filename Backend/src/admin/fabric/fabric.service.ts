import { BadRequestException, Injectable } from '@nestjs/common';
import { Fabrics } from './fabric.schema';
import { FabricsMaterial } from './fabricMaterial.schema';
import { Model } from 'sequelize-typescript';
import { Op, Sequelize } from 'sequelize';
import { InjectModel } from '@nestjs/sequelize';

interface FabricData extends Fabrics {
  fabric_images: Array<{
    id: number;
    fabric_id: number;
    color_name: string;
    color: string;
    fabric_image: string;
  }>;
}
@Injectable()
export class FabricService {
  constructor(
    @InjectModel(Fabrics) private fabricModel: typeof Fabrics,
    @InjectModel(FabricsMaterial)
    private fabricsMaterialModel: typeof FabricsMaterial,
  ) {}

  async FabricNameExist(reqBody) {
    try {
      const Fabric = await this.fabricModel.findOne({
        where: { fabric_name: reqBody.fabric_name },
        raw: true,
        nest: true,
      });

      return Fabric;
    } catch (error) {
      throw error;
    }
  }

  async CreateFabric(reqUser: any, createFabricDto: any): Promise<Fabrics> {
    try {
      const {
        fabric_name,
        material,
        ideal_for,
        feature,
        water_proof,
        uv_resistant,
        weight,
        warranty,
        fabric_type,
      } = createFabricDto;

      // Create a new category using Sequelize's create method
      const newCategory = await this.fabricModel.create({
        fabric_name: fabric_name,
        material: material,
        ideal_for: ideal_for,
        feature: feature,
        water_proof: water_proof,
        uv_resistant: uv_resistant,
        weight: weight,
        warranty: warranty,
        fabric_type: fabric_type,
        created_at: new Date(),
      });

      return newCategory;
    } catch (error) {
      console.error('Error creating Fabric:', error);
      throw new BadRequestException('Could not create Fabric.');
    }
  }

  async createFabricMaterial(
    reqUser: any,
    createTiwDownDto: any,
    fileName: any,
  ): Promise<FabricsMaterial> {
    try {
      const { fabric_id, color_name, color } = createTiwDownDto;

      // Create a new category using Sequelize's create method
      const newFabricMaterial = await this.fabricsMaterialModel.create({
        fabric_id: fabric_id,
        color_name: color_name,
        color: color,
        fabric_image: fileName,
        created_at: new Date(),
      });

      return newFabricMaterial;
    } catch (error) {
      console.error('Error creating Fabric Material:', error);
      throw new BadRequestException('Could not create Fabric Material.');
    }
  }

  async deletefabricMaterial(id) {
    try {
      // Assuming Category is your Sequelize model
      const fabricMaterial = await this.fabricsMaterialModel.destroy({
        where: { id: id, deleted_at: null },
      });

      return fabricMaterial;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  async FindfabricMaterial(id) {
    try {
      // Assuming Category is your Sequelize model
      const fabricMaterial = await this.fabricsMaterialModel.findOne({
        where: { id, deleted_at: null },
        attributes: ['fabric_image'],
        raw: true,
        nest: true,
      });

      return fabricMaterial;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  async FabricById(id) {
    var data = await this.fabricModel.findOne({
      where: { id, deleted_at: null },
      attributes: [
        'id',
        'fabric_name',
        'material',
        'ideal_for',
        'feature',
        'water_proof',
        'uv_resistant',
        'weight',
        'warranty',
        'fabric_type',
      ],
      raw: true,
      nest: true,
    });

    if (data) {
      var findMaterial = await this.fabricsMaterialModel.findAll({
        where: { fabric_id: id },
        attributes: ['id', 'fabric_id', 'color_name', 'color', 'fabric_image'],
        raw: true,
        nest: true,
      });

      // Function to prepend URL to fabric image URLs
      function prependUrlToFabricImages(images, baseUrl) {
        return images.map((image) => ({
          ...image,
          fabric_image: `${baseUrl}/${image.fabric_image}`,
        }));
      }

      // Prepend URL to fabric images
      const modifiedFabricImages = prependUrlToFabricImages(
        findMaterial,
        process.env.fabricMaterialS3Url,
      );

      // Add fabric images to the data object
      (data as FabricData).fabric_images = modifiedFabricImages;

      return data;
    }

    return data;
  }

  async allFabricFind(reqbody, reqUser) {
    try {
      let order_column = reqbody.order_column || 'id';
      let sort_order = reqbody.order_direction === 'desc' ? 'DESC' : 'ASC'; // Assuming order_direction is 'desc' or 'asc'
      let filter_value = reqbody.search || '';
      let offset =
        parseInt(reqbody.per_page) * (parseInt(reqbody.current_page) - 1);
      let limit = parseInt(reqbody.per_page) || 5;

      let order = [[order_column, sort_order]]; // Properly structured order array

      let whereClause = { deleted_at: null }; // Assuming deleted_at field

      if (filter_value) {
        whereClause[Op.or] = [
          { fabric_name: { [Op.like]: `%${filter_value}%` } },
        ];
      }

      console.log(filter_value, ':whereClause');

      const { count, rows } = await this.fabricModel.findAndCountAll({
        where: whereClause,
        attributes: [
          'id',
          'fabric_name',
          'material',
          'ideal_for',
          'feature',
          'water_proof',
          'uv_resistant',
          'weight',
          'warranty',
          'fabric_type',
        ],
        offset: offset,
        // order: order,
        limit: limit,
        raw: true,
        nest: true,
      });

      return {
        totalRecords: count,
        Febric_Listing: rows,
      };
    } catch (error) {
      console.log('Error : ', error);
      throw error; // Rethrow the error to handle it in the calling code
    }
  }

  async deleteFabric(id) {
    try {
      // Assuming Category is your Sequelize model
      const GrommetDelete = await this.fabricModel.update(
        { deleted_at: new Date() },
        {
          where: { id },
          returning: true, // To return the updated row
        },
      );

      await this.fabricsMaterialModel.update(
        { deleted_at: new Date() },
        {
          where: { fabric_id: id },
          returning: true, // To return the updated row
        },
      );

      return GrommetDelete;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  async allFabricListingVL(reqUser) {
    try {
      var data = await Fabrics.findAll({
        where: { deleted_at: null },
        attributes: ['id', 'fabric_name'],
        raw: true,
        nest: true,
      });

      const valueLabelPairs = data?.map((fabric) => {
        return { value: fabric?.id, label: fabric?.fabric_name };
      });

      return valueLabelPairs;
    } catch (error) {
      console.log('Error : ', error);
      throw error; // Rethrow the error to handle it in the calling code
    }
  }
}
