import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { Products } from './product.schema';
import { ProductsImage } from './productImage.schema';
import { Admin } from '../auth/auth.schema';
import { ProductController } from './product.controller';
import { ProductService } from './product.service';
import { EnsureAdminAuthenticatedMiddleware } from '../../../Helper/middleware';
import { S3Service } from 'Helper/S3Bucket';
@Module({
  imports: [SequelizeModule.forFeature([Products, ProductsImage, Admin])],

  controllers: [ProductController],
  providers: [ProductService, S3Service],
})
export class ProductModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(EnsureAdminAuthenticatedMiddleware)
      .forRoutes(ProductController);
  }
}
