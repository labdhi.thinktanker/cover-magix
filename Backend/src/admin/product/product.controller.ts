import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Req,
  Res,
  UseInterceptors,
  UploadedFiles,
} from '@nestjs/common';
import { ProductService } from './product.service';
import {
  CustomCatchBlockErrorMessage,
  CustomErrorResponse,
  CustomResponse,
  Success,
} from '../../../Helper/commonResponse';
import { Request, Response } from 'express';
import { FilesInterceptor } from '@nestjs/platform-express/multer';
import { S3Service } from '../../../Helper/S3Bucket';
import { Products } from './product.schema';
import { ProductsImage } from './productImage.schema';
import { CreateProduct, CreateProductImage } from './product.interface';
import { validate } from 'class-validator';
import { ValidateProduct } from './Validation/product.createValidation';
import { InjectModel } from '@nestjs/sequelize';

@Controller('product')
export class ProductController {
  constructor(
    @InjectModel(ProductsImage)
    private ProductsImageModel: typeof ProductsImage,
    private readonly s3Service: S3Service,
    private readonly productService: ProductService,
  ) {}

  isValidMySQLId(id: string): boolean {
    const regex = /^\d+$/;
    return regex.test(id);
  }

  @Post('create')
  async Productcreate(
    @Body() CreateProduct: CreateProduct,
    @Req() req: any,
    @Res() res: Response,
  ) {
    try {
      if (!req.user) {
        return new CustomErrorResponse(
          res,
          401,
          'Invalid User login',
          'Invalid Login credential',
        );
      }

      const errors = {};
      const productInput = new ValidateProduct();
      productInput.product_name = CreateProduct.product_name;
      productInput.description = CreateProduct.description;
      productInput.product_price = CreateProduct.product_price;
      productInput.meta_data = CreateProduct.meta_data;

      const validation_errors = await validate(productInput);

      const findProductExist =
        await this.productService.ProductNameExist(productInput);
      if (findProductExist) {
        errors['product_name'] = 'This Product name is already exist';
      }
      if (validation_errors.length > 0 || Object.keys(errors).length > 0) {
        validation_errors.map((error) => {
          errors[error['property']] = Object.values(error.constraints)[0];
          // UserController.final_error_object['errors'][error['property']] = Object.values(error.constraints)[0]
        });

        return new CustomErrorResponse(
          res,
          422,
          'Something went wrong',
          errors,
        );
      }

      const createdData = await this.productService.createProduct(
        req.user,
        CreateProduct,
      );
      if (createdData) {
        return new Success(
          res,
          200,
          createdData,
          '🎉 Product Created Successfully!',
        );
      } else {
        return new CustomResponse(
          res,
          400,
          createdData,
          'Something went wrong during creation',
        );
      }
    } catch (error) {
      console.log('Create Block Error -> ', error);
      return new CustomCatchBlockErrorMessage(
        res,
        500,
        error.toString(),
        'Something went wrong',
      );
    }
  }

  //productImageCreate
  @Post('image')
  @UseInterceptors(FilesInterceptor('product_image'))
  async productImageCreate(
    @Body() createProductImageDto: CreateProductImage,
    @Req() req: any,
    @Res() res: Response,
    @UploadedFiles() product_image,
  ) {
    try {
      // console.log(createProductImageDto,"data")
      if (!req.user) {
        return new CustomErrorResponse(
          res,
          401,
          'Invalid User login',
          'Invalid Login credential',
        );
      }
      const errors = {};
      const productExists = await Products.findOne({
        where: { id: createProductImageDto.product_id, deleted_at: null },
      });
      if (!productExists) {
        errors['product_id'] = 'This Product id does not exist';
        return new CustomResponse(
          res,
          400,
          errors,
          'Product id does not exist',
        );
      }

      const fileName = product_image.map((element) => {
        return `${Date.now()}-${element.originalname}`;
      });

      await this.s3Service.uploadFileToS3ForProduct(product_image, fileName);

      const createdData = await this.productService.createProductImages(
        req.user,
        createProductImageDto,
        fileName,
      );
      
      if (createdData) {
        return new Success(
          res,
          200,
          createdData,
          '🎉 Product Images added Successfully!',
        );
      } else {
        return new CustomResponse(
          res,
          400,
          createdData,
          'Something went wrong during creation',
        );
      }
    } catch (error) {
      console.log('Create Block Error -> ', error);
      return new CustomCatchBlockErrorMessage(
        res,
        500,
        error.toString(),
        'Something went wrong',
      );
    }
  }

  @Post('product_images/delete/:id')
  async productImagesDeleteByID(
    @Param('id') id: string,
    @Req() req: any,
    @Res() res: Response,
  ) {
    try {
      const ProductImageId = req.params.id;
      if (!this.isValidMySQLId(ProductImageId)) {
        return new CustomErrorResponse(
          res,
          404,
          'Enter valid ProductImage id',
          'ProductImage id isnot valid',
        );
      }

      const existingProductImage = await this.ProductsImageModel.findOne({
        where: { id, deleted_at: null },
      });
      if (!existingProductImage) {
        return new CustomErrorResponse(
          res,
          500,
          'ProductImage not found',
          'ProductImage ID doesnot found in database',
        );
      }

      const dataFindForImageDelete =
        await this.productService.ProductImagesByID(id);
      await this.s3Service.deleteProductImage(
        dataFindForImageDelete?.product_image,
      );
      const data = await this.productService.deleteproductImages(id);
      return new Success(
        res,
        200,
        {},
        '🗑️ Product Images Deleted Successfully!',
      );
    } catch (error) {
      console.log('Create Block Error -> ', error);
      return new CustomCatchBlockErrorMessage(
        res,
        500,
        error.toString(),
        'Something went wrong',
      );
    }
  }

  @Post('list')
  async getAllProductList(@Req() req: any, @Res() res: Response) {
    try {
      if (!req.user) {
        return new CustomErrorResponse(
          res,
          401,
          'Invalid User login',
          'Invalid Login credential',
        );
      }

      let product_listing = await this.productService.allProductListing(
        req.body,
        req.user,
      );

      if (product_listing) {
        return new Success(
          res,
          200,
          product_listing,
          '🎉 All Product Listed Successfully!',
        );
      } else {
        return new CustomResponse(
          res,
          400,
          product_listing,
          'Something went wrong',
        );
      }
    } catch (error) {
      console.log('Create Block Error -> ', error);
      return new CustomCatchBlockErrorMessage(
        res,
        500,
        error.toString(),
        'Something went wrong',
      );
    }
  }

  @Get('list/:id')
  async getByIdProductList(
    @Param('id') id: string,
    @Req() req: any,
    @Res() res: Response,
  ) {
    try {
      if (!req.user) {
        return new CustomErrorResponse(
          res,
          401,
          'Invalid User login',
          'Invalid Login credential',
        );
      }
      const ProductId = req.params.id;
      if (!this.isValidMySQLId(ProductId)) {
        return new CustomErrorResponse(
          res,
          404,
          'Enter valid Product id',
          'Product id isnot valid',
        );
      }

      const existingProduct = await this.productService.ProductsById(
        req.user,
        ProductId,
      );
      if (!existingProduct) {
        return new CustomErrorResponse(
          res,
          500,
          'Product not found',
          'ProductID doesnot found in database',
        );
      }
      return new Success(
        res,
        200,
        existingProduct,
        '🔍 Product Found Successfully!',
      );
    } catch (error) {
      console.log('Create Block Error -> ', error);
      return new CustomCatchBlockErrorMessage(
        res,
        500,
        error.toString(),
        'Something went wrong',
      );
    }
  }

  @Post('delete/:id')
  async ProductDeleteById(
    @Param('id') id: string,
    @Req() req: any,
    @Res() res: Response,
  ) {
    try {
      const ProductId = req.params.id;
      if (!this.isValidMySQLId(ProductId)) {
        return new CustomErrorResponse(
          res,
          404,
          'Enter valid Product id',
          'Product id isnot valid',
        );
      }

      const existingProduct = await this.productService.ProductsById(
        req.user,
        ProductId,
      );
      if (!existingProduct) {
        return new CustomErrorResponse(
          res,
          500,
          'Product not found',
          'ProductID doesnot found in database',
        );
      }

      const data = await this.productService.deleteProduct(id);
      if (data) {
        return new Success(res, 200, {}, '🗑️ Product Deleted Successfully!');
      } else {
        return new CustomResponse(
          res,
          400,
          data,
          'Something went wrong during Serach',
        );
      }
    } catch (error) {
      console.log('Create Block Error -> ', error);
      return new CustomCatchBlockErrorMessage(
        res,
        500,
        error.toString(),
        'Something went wrong',
      );
    }
  }

  @Post('update/:id')
  async updateProductById(
    @Param('id') id: string,
    @Req() req: any,
    @Res() res: Response,
  ) {
    try {
      if (!req.user) {
        return new CustomErrorResponse(
          res,
          401,
          'Invalid User login',
          'Invalid Login credential',
        );
      }

      const ProductId = req.params.id;
      if (!this.isValidMySQLId(ProductId)) {
        return new CustomErrorResponse(
          res,
          404,
          'Enter valid Product id',
          'Product id isnot valid',
        );
      }

      const existingProduct = await this.productService.ProductsById(
        req.user,
        ProductId,
      );
      if (!existingProduct) {
        return new CustomErrorResponse(
          res,
          500,
          'Product not found',
          'ProductID doesnot found in database',
        );
      }

      const errors = {};
      const productInput = new ValidateProduct();
      productInput.product_name = req.body.product_name;
      productInput.description = req.body.description;
      productInput.product_price = req.body.product_price;
      productInput.meta_data = req.body.meta_data;

      const validation_errors = await validate(productInput);

      if (validation_errors.length > 0 || Object.keys(errors).length > 0) {
        validation_errors.map((error) => {
          errors[error['property']] = Object.values(error.constraints)[0];
          // UserController.final_error_object['errors'][error['property']] = Object.values(error.constraints)[0]
        });

        return new CustomErrorResponse(
          res,
          422,
          'Something went wrong',
          errors,
        );
      }

      const updateData = await this.productService.updateProduct(
        req.user,
        id,
        req.body,
      );
      if (updateData) {
        return new Success(res, 200, true, '🎊 Product Updated Successfully!');
      } else {
        return new CustomResponse(
          res,
          400,
          false,
          'Something went wrong during Serach',
        );
      }
    } catch (error) {
      console.log('Create Block Error -> ', error);
      return new CustomCatchBlockErrorMessage(
        res,
        500,
        error.toString(),
        'Something went wrong',
      );
    }
  }

  @Get('list_V_L')
  async getAllSubcategoryListValueLabelWise(
    @Req() req: any,
    @Res() res: Response,
  ) {
    try {
      if (!req.user) {
        return new CustomErrorResponse(
          res,
          401,
          'Invalid User login',
          'Invalid Login credential',
        );
      }

      let subcategories_listing_VL =
        await this.productService.allProductsListingVL(req.user);

      if (subcategories_listing_VL) {
        return new Success(
          res,
          200,
          subcategories_listing_VL,
          '📋 All Products Listed Successfully by Label and Value!',
        );
      } else {
        return new CustomResponse(
          res,
          400,
          subcategories_listing_VL,
          'Something went wrong',
        );
      }
    } catch (error) {
      console.log('Create Block Error -> ', error);
      return new CustomCatchBlockErrorMessage(
        res,
        500,
        error.toString(),
        'Something went wrong',
      );
    }
  }
}
