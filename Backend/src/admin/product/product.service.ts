import { BadRequestException, Injectable } from '@nestjs/common';
import { Model } from 'sequelize-typescript';
import { InjectModel } from '@nestjs/sequelize';
import { Op, Sequelize } from 'sequelize';
import { Products } from './product.schema';
import { ProductsImage } from './productImage.schema';
import { Sub_Categories } from '../sub_categories/sub_categories.schema';
import { Categories } from '../categories/categories.schema';

@Injectable()
export class ProductService {
  constructor(
    @InjectModel(Products) private productModel: typeof Products,
    @InjectModel(ProductsImage)
    private ProductsImageModel: typeof ProductsImage,
  ) {}

  async ProductNameExist(reqBody) {
    try {
      const products = await this.productModel.findOne({
        where: { product_name: reqBody.product_name, deleted_at: null },
        raw: true,
        nest: true,
      });

      return products;
    } catch (error) {
      throw error;
    }
  }

  async createProduct(reqUser: any, createProductDto: any): Promise<Products> {
    try {
      const {
        category_id,
        sub_category_id,
        product_name,
        description,
        product_price,
        meta_data,
      } = createProductDto;

      // Create a new category using Sequelize's create method
      const newProduct = await this.productModel.create({
        category_id: category_id,
        sub_category_id: sub_category_id,
        product_name: product_name,
        description: description,
        product_price: product_price,
        meta_data: meta_data,
        created_at: new Date(),
      });

      return newProduct;
    } catch (error) {
      console.error('Error creating product:', error);
      throw new BadRequestException('Could not create product.');
    }
  }

  async createProductImages(
    reqUser: any,
    createTiwDownDto: any,
    fileName: any,
  ){
    try {
      const { product_id } = createTiwDownDto;
  
      const existingProductImage = await this.ProductsImageModel.findOne({
        where: { product_id: product_id },
      });
  
      if (existingProductImage) {
        // Update the existing product image
        const updatedProductImage = await this.ProductsImageModel.update(
          {
            product_image:  [...existingProductImage.product_image, fileName].flat(),
            updated_at: new Date(),
          },
          {
            where: { product_id: product_id, deleted_at: null },
          },
        );
  
        return updatedProductImage; // Return the result of update operation
      } else {
        // Create a new product image
        const newProductImage = await this.ProductsImageModel.create({
          product_id: product_id,
          product_image: fileName,
          created_at: new Date(),
        });
  
        return newProductImage; // Return the newly created product image
      }
    } catch (error) {
      console.error('Error creating Product Image:', error);
      throw new BadRequestException('Could not create Product Image.');
    }
  }
  

  async ProductImagesByID(id) {
    try {
      // Assuming Category is your Sequelize model
      const productImages = await this.ProductsImageModel.findOne({
        where: { id },
        attributes: ['product_image'],
        raw: true,
        nest: true,
      });

      return productImages;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  async deleteproductImages(id) {
    try {
      // Assuming Category is your Sequelize model
      const productImages = await this.ProductsImageModel.destroy({
        where: { id,deleted_at:null },
      });

      return productImages;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  async allProductListing(reqbody, reqUser) {
    try {
      let order_column = reqbody.order_column || 'id';
      let sort_order = reqbody.order_direction === 'desc' ? 'DESC' : 'ASC'; // Assuming order_direction is 'desc' or 'asc'
      let filter_value = reqbody.search || '';
      let offset =
        parseInt(reqbody.per_page) * (parseInt(reqbody.current_page) - 1);
      let limit = parseInt(reqbody.per_page) || 5;

      let order = [[order_column, sort_order]]; // Properly structured order array

      let whereClause = { deleted_at: null }; // Assuming deleted_at field

      if (filter_value) {
        whereClause[Op.or] = [
          { product_name: { [Op.like]: `%${filter_value}%` } },
        ];
      }

      console.log(filter_value, ':whereClause');

      const { count, rows } = await Products.findAndCountAll({
        where: whereClause,
        include: [
          { model: Categories, attributes: ['category_name'] },
          { model: Sub_Categories, attributes: ['sub_category_name'] },
        ],
        attributes: [
          'id',
          'category_id',
          'sub_category_id',
          'product_name',
          'description',
          'product_price',
          'meta_data',
        ],
        offset: offset,
        // order: order,
        limit: limit,
        raw: true,
        nest: true,
      });

      return {
        totalRecords: count,
        Product_listing: rows,
      };
    } catch (error) {
      console.log('Error : ', error);
      throw error; // Rethrow the error to handle it in the calling code
    }
  }

  async ProductsById(reqUser, id) {
    try {
      const data = await this.productModel.findOne({
        where: { id, deleted_at: null },
        attributes: [
          'id',
          'category_id',
          'sub_category_id',
          'product_name',
          'description',
          'product_price',
          'meta_data',
        ],
      });
      return data;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  async deleteProduct(id) {
    try {
      // Assuming Category is your Sequelize model
      const product = await this.productModel.update(
        { deleted_at: new Date() },
        {
          where: { id },
          returning: true, // To return the updated row
        },
      );
      return product;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  async updateProduct(reqUser, id, reqBody) {
    try {
      const updatedProduct = await this.productModel.update(
        {
          category_id: reqBody.category_id,
          sub_category_id: reqBody.sub_category_id,
          product_name: reqBody.product_name,
          description: reqBody.description,
          product_price: reqBody.product_price,
          meta_data: reqBody.meta_data,
          updated_at: new Date(),
        },
        {
          returning: true,
          where: { id: id, deleted_at: null },
        },
      );

      // The updatedCategory variable contains the number of affected rows
      // and possibly the updated rows if the "returning" option is set.

      return updatedProduct;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  async allProductsListingVL(reqUser) {
    try {
      var data = await Products.findAll({
        where: { deleted_at: null },
        attributes: ['id', 'product_name'],
        raw: true,
        nest: true,
      });

      const valueLabelPairs = data?.map((product) => {
        return { value: product?.id, label: product?.product_name };
      });

      return valueLabelPairs;
    } catch (error) {
      console.log('Error : ', error);
      throw error; // Rethrow the error to handle it in the calling code
    }
  }
}
