import {IsEmail,IsNotEmpty} from 'class-validator';


export class ValidateProduct {

    @IsNotEmpty({
        message:"Product Name is required"
      })
      product_name : string;

      @IsNotEmpty({
        message:"Product Description is required."
      })
      description:string;

      @IsNotEmpty({
        message:"Product Price is required."
      })
      product_price:string;

      @IsNotEmpty({
        message:"Product MetaData is required."
      })
      meta_data:string;
}
