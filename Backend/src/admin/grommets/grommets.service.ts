import { BadRequestException, Injectable } from '@nestjs/common';
import { Grommets } from './grommets.schema';
import { InjectModel } from '@nestjs/sequelize';
import { Op, Sequelize } from 'sequelize';
import * as ExcelJS from 'exceljs';
import { config } from 'dotenv';
import { ValidationError } from 'Helper/commonResponse';
import { S3Service } from '../../../Helper/S3Bucket';
import * as path from 'path';

config();

@Injectable()
export class GrommetsService {
  constructor(
    @InjectModel(Grommets) private grommetModel: typeof Grommets,
    private readonly s3Service: S3Service,
  ) { }


  async createGrommet(
    reqUser: any,
    createCategoryDto: any,
    fileName: any
  ): Promise<Grommets> {
    try {
      const { grommet_name, price } = createCategoryDto;

      // Create a new category using Sequelize's create method
      const newGrommet = await this.grommetModel.create({
        grommet_name: grommet_name,
        price: price,
        grommet_image: fileName,
        created_at: new Date()
      });

      return newGrommet;
    } catch (error) {
      console.error('Error creating category:', error);
      throw new BadRequestException('Could not create category.');
    }
  }

  async GrommetNameExist(reqBody) {
    try {
      const Grommets = await this.grommetModel.findOne({
        where: { grommet_name: reqBody.grommet_name },
        raw: true,
        nest: true,
      });

      return Grommets;
    } catch (error) {
      throw error;
    }
  }

  async allGrommetListing(reqbody, reqUser) {
    try {
      let order_column = reqbody.order_column || 'id';
      let sort_order = reqbody.order_direction === 'desc' ? 'DESC' : 'ASC'; // Assuming order_direction is 'desc' or 'asc'
      let filter_value = reqbody.search || '';
      let offset =
        parseInt(reqbody.per_page) * (parseInt(reqbody.current_page) - 1);
      let limit = parseInt(reqbody.per_page) || 5;

      let order = [[order_column, sort_order]]; // Properly structured order array

      let whereClause = { deleted_at: null }; // Assuming deleted_at field

      if (filter_value) {
        whereClause[Op.or] = [
          { grommet_name: { [Op.like]: `%${filter_value}%` } },
        ];
      }

      const { count, rows } = await this.grommetModel.findAndCountAll({
        where: whereClause,
        attributes: ['id', 'grommet_name', 'price', 'grommet_image'],
        offset: offset,
        // order: order,
        limit: limit,
        raw: true,
        nest: true,
      });

      const modifiedRows = rows.map(row => {
        row.grommet_image = `${process.env.GrommetS3Url}/${row.grommet_image}`;
        row.price = `$${row.price}`;
        return row;
      });

      return {
        totalRecords: count,
        Grommet_listing: modifiedRows,
      };
    } catch (error) {
      console.log('Error : ', error);
      throw error; // Rethrow the error to handle it in the calling code
    }
  }

  async GrommetById(reqUser, id) {
    try {
      const data = await this.grommetModel.findOne({ where: { id , deleted_at : null}, attributes: ['id', 'grommet_name', 'price', 'grommet_image'] });
      return {
        id: data.id,
        grommet_name: data.grommet_name,
        price: `$${data.price}`,
        grommet_image: `${process.env.GrommetS3Url}/${data.grommet_image}`,
      };
      // return data;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  async deleteGrommet(id) {
    try {
      // Assuming Category is your Sequelize model
      const GrommetDelete = await this.grommetModel.update(
        { deleted_at: new Date() },
        {
          where: { id },
          returning: true // To return the updated row
        }
      );


      return GrommetDelete
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  async allGrommetListingVL(reqUser) {
    try {
      var data = await Grommets.findAll({
        where: { deleted_at: null },
        attributes: ['id', 'grommet_name'],
        raw: true,
        nest: true,
      });

      const valueLabelPairs = data?.map(grommet => {
        return { value: grommet?.id, label: grommet?.grommet_name };
      });

      return valueLabelPairs;
    } catch (error) {
      console.log('Error : ', error);
      throw error; // Rethrow the error to handle it in the calling code
    }
  }

  // async importExcel(data) {
  //   try {
  //     const newData = data.map((item, index) => {
  //       if (!item.grommet_name) {
  //         throw new ValidationError(422, `There is an error in line ${index + 2}. Please provide a valid Grommet Name`);
  //       } else if (!item.price) {
  //         throw new ValidationError(422, `There is an error in line ${index + 2}. Please provide a valid Price`);
  //       } else if (!item.grommet_image) {
  //         throw new ValidationError(422, `There is an error in line ${index + 2}. Please provide a valid Image`);
  //       }

  //       return {
  //         grommet_name: item.grommet_name,
  //         price: item.price,
  //         grommet_image: item.grommet_image
  //       };
  //     });

  //     const result = await this.grommetModel.bulkCreate(newData);
  //     return result;
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  async importExcel(data) {
    try {
      const newData = [];

      for (let i = 0; i < data.length; i++) {
        const item = data[i];

        if (!item.grommet_name) {
          throw new ValidationError(422, `There is an error in line ${i + 1}. Please provide a valid Grommet Name`);
        } else if (!item.price) {
          throw new ValidationError(422, `There is an error in line ${i + 1}. Please provide a valid Price`);
        } else if (!item.grommet_image) {
          throw new ValidationError(422, `There is an error in line ${i + 1}. Please provide a valid Image URL`);
        }

        const imageKey = `${Date.now()}-${path.basename(item.grommet_image)}`;
        const imageUrl = await this.s3Service.uploadFileToS3FromURL(item.grommet_image, imageKey);

        newData.push({
          grommet_name: item.grommet_name,
          price: item.price,
          grommet_image: imageKey
        });
      }

      const result = await this.grommetModel.bulkCreate(newData);
      return result;
    } catch (error) {
      throw error;
    }
  }

  async updateGrommets(reqUser, id, reqBody, file) {
    try {
      const updateGrommet = await this.grommetModel.update(
        {
          grommet_name: reqBody.grommet_name,
          price: reqBody.price,
          grommet_image:file
        },
        { where: { id: id ,deleted_at: null} }
      );
      return updateGrommet;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

 

}
