import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
// import { Sub_Categories } from './sub_categories.schema';
import { GrommetsController } from './grommets.controller';
import { GrommetsService } from './grommets.service';
import { Admin } from '../auth/auth.schema';
import { EnsureAdminAuthenticatedMiddleware } from '../../../Helper/middleware';
import { Grommets } from './grommets.schema';
import { S3Service } from 'Helper/S3Bucket';

@Module({
    imports: [
      SequelizeModule.forFeature([Grommets , Admin]), 
      
    ],
  
    controllers: [GrommetsController],
    providers: [GrommetsService,S3Service],
  })
  
export class GrommetsModule  implements NestModule {
    configure(consumer: MiddlewareConsumer) {
      consumer
        .apply(EnsureAdminAuthenticatedMiddleware)
        .forRoutes(GrommetsController);    
    }
  }
