import { BadRequestException, Injectable } from '@nestjs/common';
import { DeckType } from './deck_type.schema';
import { InjectModel } from '@nestjs/sequelize';
import { Op, Sequelize } from 'sequelize';
import { config } from 'dotenv';

@Injectable()
export class DeckTypeService {
    constructor(
        @InjectModel(DeckType) private DeckModel: typeof DeckType,
      ) {}


      async createDeckType(
        reqUser: any,
        createDeckDto: any,
        fileName : any
      ): Promise<DeckType> {
        try {
          const { deck_name , price } = createDeckDto;
    
          // Create a new category using Sequelize's create method
          const newGrommet = await this.DeckModel.create({
            deck_name: deck_name,
            price : price,
            deck_image : fileName,
            created_at : new Date()
          });
    
          return newGrommet;
        } catch (error) {
          console.error('Error creating Deck Type:', error);
          throw new BadRequestException('Could not Deck Type.');
        }
      }

      async DeckNameExist(reqBody) {
        try {
          const DeckData = await this.DeckModel.findOne({
            where: { deck_name: reqBody.deck_name , deleted_at:null },
            raw: true,
            nest: true,
          });
    
          return DeckData;
        } catch (error) {
          throw error;
        }
      }

      async allDeckListing(reqbody, reqUser) {
        try {
          let order_column = reqbody.order_column || 'id';
          let sort_order = reqbody.order_direction === 'desc' ? 'DESC' : 'ASC'; // Assuming order_direction is 'desc' or 'asc'
          let filter_value = reqbody.search || '';
          let offset =
            parseInt(reqbody.per_page) * (parseInt(reqbody.current_page) - 1);
          let limit = parseInt(reqbody.per_page) || 5;
    
          let order = [[order_column, sort_order]]; // Properly structured order array
    
          let whereClause = { deleted_at: null }; // Assuming deleted_at field
    
          if (filter_value) {
            whereClause[Op.or] = [
              { deck_name : { [Op.like]: `%${filter_value}%` } },
            ];
          }
    
          const { count, rows } = await this.DeckModel.findAndCountAll({
            where: whereClause,
            attributes:['id' , 'deck_name' , 'price' , 'deck_image'],
            offset: offset,
            // order: order,
            limit: limit,
            raw: true,
            nest: true,
          });

          const modifiedRows = rows.map(row => {
            row.deck_image = `${process.env.DeckTypeS3Url}/${row.deck_image}`;
            row.price = `$${row.price}`;
            return row;
        });
    
          return {
            totalRecords: count,
            Deck_listing: modifiedRows,
          };
        } catch (error) {
          console.log('Error : ', error);
          throw error; // Rethrow the error to handle it in the calling code
        }
      }

      async DeckById(reqUser, id) {
        try {
          const data = await this.DeckModel.findOne({ where: { id , deleted_at : null }, attributes:['id' , 'deck_name' , 'price' , 'deck_image'] });
          return {
            id : data.id,
            deck_name : data.deck_name,
            price : `$${data.price}`,
            deck_image :  `${process.env.DeckTypeS3Url}/${data.deck_image}`,
          };
        } catch (error) {
          console.log(error);
          throw error;
        }
      }

      async deleteDeck(id) {
        try {
          // Assuming Category is your Sequelize model
          const deckDeleted = await this.DeckModel.update(
            { deleted_at: new Date() },
            { 
              where: { id },
              returning: true // To return the updated row
            }
          );
      
          
          return deckDeleted
        } catch (error) {
          console.log(error);
          throw error;
        }
      }

      async allDeckTypeListingVL(reqUser) {
        try {
          var data = await DeckType.findAll({
            where: { deleted_at: null },
            attributes: ['id', 'deck_name'],
            raw: true,
            nest: true,
          });
    
          const valueLabelPairs = data?.map(deck_type => {
            return { value: deck_type?.id, label: deck_type?.deck_name };
          });
    
          return valueLabelPairs;
        } catch (error) {
          console.log('Error : ', error);
          throw error; // Rethrow the error to handle it in the calling code
        }
      }

      async updateDeckTypes(reqUser, id, reqBody, file) {
        try {
          const updateDeckType = await this.DeckModel.update(
            {
              deck_name: reqBody.deck_name,
              price: reqBody.price,
              deck_image:file
            },
            { where: { id: id } }
          );
          return updateDeckType;
        } catch (error) {
          console.log(error);
          throw error;
        }
      }
}
