import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { DeckTypeController } from './deck_type.controller';
import { DeckTypeService } from './deck_type.service';
import { Admin } from '../auth/auth.schema';
import { DeckType } from './deck_type.schema';
import { S3Service } from 'Helper/S3Bucket';
import { EnsureAdminAuthenticatedMiddleware } from '../../../Helper/middleware';

@Module({
    imports: [
        SequelizeModule.forFeature([DeckType, Admin]), 
      ],
      controllers: [DeckTypeController],
      providers: [DeckTypeService,S3Service],
})


export class DeckTypeModule implements NestModule{
    configure(consumer: MiddlewareConsumer) {
        consumer
          .apply(EnsureAdminAuthenticatedMiddleware)
          .forRoutes(DeckTypeController);    
      }
}
