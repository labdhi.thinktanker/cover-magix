import { BadRequestException, Injectable } from '@nestjs/common';
import { Model } from 'sequelize-typescript';
import { InjectModel } from '@nestjs/sequelize';
import { Op, Sequelize } from 'sequelize';
import { Sub_Categories } from './sub_categories.schema';
import { Categories } from '../categories/categories.schema';

@Injectable()
export class SubCategoriesService {
    constructor(
        @InjectModel(Sub_Categories) private subcategoryModel: typeof Sub_Categories,
      ) {}

      async SubCategoryNameExist(reqBody) {
        try {
          const categories = await this.subcategoryModel.findOne({
            where: { sub_category_name: reqBody.sub_category_name , deleted_at : null },
            raw: true,
            nest: true,
          });
    
          return categories;
        } catch (error) {
          throw error;
        }
      }

      async createCategory(
        reqUser: any,
        createSubCategoryDto: any,
      ): Promise<Sub_Categories> {
        try {
          const { category_id , sub_category_name } = createSubCategoryDto;
    
          // Create a new category using Sequelize's create method
          const newSubCategory = await this.subcategoryModel.create({
            category_id : category_id,
            sub_category_name: sub_category_name,
            created_at : new Date()
          });
    
          return newSubCategory;
        } catch (error) {
          console.error('Error creating category:', error);
          throw new BadRequestException('Could not create category.');
        }
      }

      async allSubCategoryListing(reqbody, reqUser) {
        try {
          let order_column = reqbody.order_column || 'id';
          let sort_order = reqbody.order_direction === 'desc' ? 'DESC' : 'ASC'; // Assuming order_direction is 'desc' or 'asc'
          let filter_value = reqbody.search || '';
          let offset =
            parseInt(reqbody.per_page) * (parseInt(reqbody.current_page) - 1);
          let limit = parseInt(reqbody.per_page) || 5;
    
    
          let order = [[order_column, sort_order]]; // Properly structured order array
    
          let whereClause = { deleted_at: null }; // Assuming deleted_at field
    
          if (filter_value) {
            whereClause[Op.or] = [
              { sub_category_name: { [Op.like]: `%${filter_value}%` }  , deleted_at : null},
              
            ];
          }
    
          const { count, rows } = await Sub_Categories.findAndCountAll({
            where: whereClause,
            include:[
              { model: Categories  , attributes : ['category_name']}
            ],
            attributes : ['id' , 'category_id' ,'sub_category_name' ],
            offset: offset,
            // order: order,
            limit: limit,
            raw: true,
            nest: true,
          });
    
          return {
            totalRecords: count,
            Sub_Categorie_listing: rows,
          };
        } catch (error) {
          console.log('Error : ', error);
          throw error; // Rethrow the error to handle it in the calling code
        }
      }

      async SubCategoriesById(reqUser, id) {
        try {
          const data = await this.subcategoryModel.findOne({ where: { id , deleted_at : null} ,  attributes : ['id' , 'category_id' ,'sub_category_name' ] });
          return data;
        } catch (error) {
          console.log(error);
          throw error;
        }
      }


      async deleteSubCategorie(id) {
        try {
          // Assuming Category is your Sequelize model
          const category = await this.subcategoryModel.update(
            { deleted_at: new Date() },
            { 
              where: { id },
              returning: true // To return the updated row
            }
          );
          return category
        } catch (error) {
          console.log(error);
          throw error;
        }
      }

      async updateSubCategories(reqUser, id, reqBody) {
        try {
            const updatedSubCategory = await this.subcategoryModel.update(
                {
                  category_id : reqBody.category_id,
                    sub_category_name : reqBody.sub_category_name,
                    updated_at: new Date()
                },
                { 
                    returning: true, 
                    where: { id: id, deleted_at: null } 
                }
            );
    
            // The updatedCategory variable contains the number of affected rows
            // and possibly the updated rows if the "returning" option is set.
    
            return updatedSubCategory;
        } catch (error) {
            console.log(error);
            throw error;
        }
    }

    async allSubCategoriesListingVL(reqUser) {
      try {
        var data = await Sub_Categories.findAll({
          where: { deleted_at: null },
          attributes: ['id', 'sub_category_name'],
          raw: true,
          nest: true,
        });
  
        const valueLabelPairs = data?.map(subcategory => {
          return { value: subcategory?.id, label: subcategory?.sub_category_name };
        });
  
        return valueLabelPairs;
      } catch (error) {
        console.log('Error : ', error);
        throw error; // Rethrow the error to handle it in the calling code
      }
    }

    async SubCategoriesListingVLByID(reqUser,id) {
      try {
        var data = await Sub_Categories.findAll({
          where: { id: null },
          attributes: ['id', 'sub_category_name'],
          raw: true,
          nest: true,
        });
  
        const valueLabelPairs = data?.map(subcategory => {
          return { value: subcategory?.id, label: subcategory?.sub_category_name };
        });
  
        return valueLabelPairs;
      } catch (error) {
        console.log('Error : ', error);
        throw error; // Rethrow the error to handle it in the calling code
      }
    }

}
