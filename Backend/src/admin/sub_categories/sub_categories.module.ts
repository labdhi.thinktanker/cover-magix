import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { Sub_Categories } from './sub_categories.schema';
import { Admin } from '../auth/auth.schema';
import { SubCategoriesController } from './sub_categories.controller';
import { SubCategoriesService } from './sub_categories.service';
import { EnsureAdminAuthenticatedMiddleware } from '../../../Helper/middleware';

@Module({
    imports: [
      SequelizeModule.forFeature([Sub_Categories , Admin]), 
    ],
  
    controllers: [SubCategoriesController],
    providers: [SubCategoriesService],
  })
  
export class SubCategoriesModule  implements NestModule {
    configure(consumer: MiddlewareConsumer) {
      consumer
        .apply(EnsureAdminAuthenticatedMiddleware)
        .forRoutes(SubCategoriesController);    
    }
  }
