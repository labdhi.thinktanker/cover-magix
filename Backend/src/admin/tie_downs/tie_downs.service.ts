import { BadRequestException, Injectable } from '@nestjs/common';
import { Tie_Down } from './tie_downs.schema';
import { InjectModel } from '@nestjs/sequelize';
import { Op, Sequelize } from 'sequelize';
import { config } from 'dotenv';
config();

@Injectable()
export class TieDownsService {
    constructor(
        @InjectModel(Tie_Down) private tieDownModel: typeof Tie_Down,
      ) {}

      async createTieDown(
        reqUser: any,
        createTiwDownDto: any,
        fileName : any
      ): Promise<Tie_Down> {
        try {
          const { tie_down_name , price } = createTiwDownDto;
    
          // Create a new category using Sequelize's create method
          const newGrommet = await this.tieDownModel.create({
            tie_down_name: tie_down_name,
            price : price,
            tie_down_image : fileName,
            created_at : new Date()
          });
    
          return newGrommet;
        } catch (error) {
          console.error('Error creating category:', error);
          throw new BadRequestException('Could not create category.');
        }
      }

      async TieDownNameExist(reqBody) {
        try {
          const Tie_Down = await this.tieDownModel.findOne({
            where: { tie_down_name: reqBody.tie_down_name ,deleted_at : null},
            raw: true,
            nest: true,
          });
    
          return Tie_Down;
        } catch (error) {
          throw error;
        }
      }
      
      async alltieDownListing(reqbody, reqUser) {
        try {
          let order_column = reqbody.order_column || 'id';
          let sort_order = reqbody.order_direction === 'desc' ? 'DESC' : 'ASC'; // Assuming order_direction is 'desc' or 'asc'
          let filter_value = reqbody.search || '';
          let offset =
            parseInt(reqbody.per_page) * (parseInt(reqbody.current_page) - 1);
          let limit = parseInt(reqbody.per_page) || 5;
    
          let order = [[order_column, sort_order]]; // Properly structured order array
    
          let whereClause = { deleted_at: null }; // Assuming deleted_at field
    
          if (filter_value) {
            whereClause[Op.or] = [
              { tie_down_name: { [Op.like]: `%${filter_value}%` } },
            ];
          }
    
          const { count, rows } = await this.tieDownModel.findAndCountAll({
            where: whereClause,
            attributes:['id' , 'tie_down_name' , 'price' , 'tie_down_image'],
            offset: offset,
            // order: order,
            limit: limit,
            raw: true,
            nest: true,
          });

          const modifiedRows = rows.map(row => {
            row.tie_down_image = `${process.env.TieDownS3Url}/${row.tie_down_image}`;
            row.price = `$${row.price}`;
            return row;
        });
    
          return {
            totalRecords: count,
            TieDown_listing: modifiedRows,
          };
        } catch (error) {
          console.log('Error : ', error);
          throw error; // Rethrow the error to handle it in the calling code
        }
      }

      async TieDownById(reqUser, id) {
        try {
          const data = await this.tieDownModel.findOne({ where: { id }, attributes:['id' , 'tie_down_name' , 'price' , 'tie_down_image'] });
          return {
            id : data.id,
            tie_down_name : data.tie_down_name,
            price : `$${data.price}`,
            tie_down_image :  `${process.env.GrommetS3Url}/${data.tie_down_image}`,
          };
        } catch (error) {
          console.log(error);
          throw error;
        }
      }

      async deleteTieDown(id) {
        try {
          // Assuming Category is your Sequelize model
          const GrommetDelete = await this.tieDownModel.update(
            { deleted_at: new Date() },
            { 
              where: { id },
              returning: true // To return the updated row
            }
          );
      
          
          return GrommetDelete
        } catch (error) {
          console.log(error);
          throw error;
        }
      }

      async allTiedownListingVL(reqUser) {
        try {
          var data = await Tie_Down.findAll({
            where: { deleted_at: null },
            attributes: ['id', 'tie_down_name'],
            raw: true,
            nest: true,
          });
    
          const valueLabelPairs = data?.map(tie_down => {
            return { value: tie_down?.id, label: tie_down?.tie_down_name };
          });
    
          return valueLabelPairs;
        } catch (error) {
          console.log('Error : ', error);
          throw error; // Rethrow the error to handle it in the calling code
        }
      }

      async updateTieDown(reqUser, id, reqBody, file) {
        try {
          const updateTieDowns = await this.tieDownModel.update(
            {
              tie_down_name: reqBody.tie_down_name,
              price: reqBody.price,
              tie_down_image:file
            },
            { where: { id: id } }
          );
          return updateTieDowns;
        } catch (error) {
          console.log(error);
          throw error;
        }
      }
    
}
