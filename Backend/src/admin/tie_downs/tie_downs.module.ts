import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { Admin } from '../auth/auth.schema';
import { EnsureAdminAuthenticatedMiddleware } from '../../../Helper/middleware';
import { Tie_Down } from './tie_downs.schema';
import { S3Service } from 'Helper/S3Bucket';
import { TieDownsController } from './tie_downs.controller';
import { TieDownsService } from './tie_downs.service';

@Module({
    imports: [
      SequelizeModule.forFeature([Tie_Down , Admin]), 
      
    ],
  
    controllers: [TieDownsController],
    providers: [TieDownsService,S3Service],
  })

export class TieDownsModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
      consumer
        .apply(EnsureAdminAuthenticatedMiddleware)
        .forRoutes(TieDownsController);    
    }
  }
